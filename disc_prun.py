#In this module we define most of the algorithms from https://eprint.iacr.org/2017/155.pdf and
#
#concerning discrete enumeration
from math import ceil,floor
from numpy.linalg import norm
import numpy as np
import fpylll as fp
import time
def tagging_np(x):
    #x is a vector of some linear combination of gram schmidt vectors of a lattice
    # outputs a list, t, such that x is in the cell C_N(t)
    #algorithm 2 in the above paper
    t = []
    for i in range(len(x))[::1]:
        if x[i]>0: t.append(ceil(2*x[i]) - 1)
        else: t.append(-1*floor(2*x[i]))
    return t



def natural_partition( tag, basis_matrix ):
    U = np.linalg.qr(basis_matrix)[0]
    n = len(basis_matrix)
    print(n)
    coefficients = [0 for _ in range(n)]
    coefficients[n-1] = (-1)**(tag[n-1])*ceil(tag[n-1]/2)
    index_list = [i for i in range(n)][-2::-1] #skip the last index (n-1) and go down to index 0
    for i in index_list:
        y = -1 * sum([coefficients[j]*U[j,i] for j in range(i+1,n)])
        coefficients[i] = floor(y+0.5)
        if coefficients[i] < y:coefficients[i]= coefficients[i] - (-1)**(tag[i])*ceil(tag[i]/2)
        else: coefficients[i] = coefficients[i] + (-1)**(tag[i])*ceil(tag[i]/2)
    combination = np.array([coefficients[i]*np.array(basis_matrix[i]) for i in range(n)])
    return combination.sum(axis = 0)


# Here are all the algorithms used in https://eprint.iacr.org/2018/546.pdf


def f1(i,t_i,lengths_list):
    if t_i == 0: return 0
    else: return (t_i**2/4.0+t_i/4.0+1/12.0)*(lengths_list[i]**2)#caution about the index

def fspec(i,t_i,lengths_list):
    if t_i == 0: return 0
    else: return (t_i*(lengths_list[i]))#caution about the index

def f2(i,t_i,lengths_list):
    if t_i == 0: return 0
    else: return (t_i**2+t_i)*(lengths_list[i]**2)#caution about the index

def enum_lowcost(f, R,n, lengths_list):
    #takes as input a function f and cound R as input
    #outputs all tags where sum f(i,v_i) from i =1 to n is <= R
    pk_list = [0 for i in range(n+1)]
    solutions =[]
    v = [0 for i in range(n)]
    k = n-1 #indexing differs from that  of paper
    while True:
        pk_list[k] = pk_list[k+1] + f(k,v[k],lengths_list)
        if pk_list[k] <= R:
            if k == 0:
                solutions.append(tuple(v))
                v[k] = v[k]+1
            else: k,v[k] = k-1, 0
        else:
            k = k+1
            if k == n:
                print('Solutions are: ',solutions)
                return solutions
            else: v[k] = v[k]+1

def count_lc_assignments(f,R,n, lengths_list,M):
    #takes as input a function f, bound R, and number M>=0
    #Decides if the number of tags such that sum f(i,v_i)<=R is >=M(labeled False) or < M (Labeled True)
    pk_list = [0 for i in range(n+1)]
    v = [0 for i in range(n)]
    k = n-1 #indexing differs from that  of paper
    m = 0
    while m<M:
        pk_list[k] = pk_list[k+1] + f(k,v[k],lengths_list)
        if pk_list[k] <= R:
            if k == 0:
                m += 1
                v[k] = v[k]+1
            else: k,v[k] = k-1, 0
        else:
            k = k+1
            if k == n:
                return True
            else: v[k] = v[k]+1
    return False

def enumeration_lowcost_assignments(f,n, lengths_list,M):
    R_0,R_1 = 0,0
    for i in range(n): R_1 += f(i,ceil(M**(1/n)), lengths_list)
    while R_0 < R_1-1:
        R = floor((R_0+R_1)/2)
        if count_lc_assignments(f,R,n,lengths_list,M) ==False: R_1 = R
        else: R_0 = R
    print('Minimized R: '+ str(R_1))
    return enum_lowcost(f, R_1,n, lengths_list)

def get_gramschmidt_lengths(rbasis):
    n = len(rbasis)
    B = fp.IntegerMatrix.from_matrix(rbasis)
    G = fp.GSO.Mat(B)
    G.update_gso()
    return [G.get_r(i,i) for i in range(n)]


def enum_lowcost_fixed( R, rbasis):
    n = len(rbasis)
    alphas = get_gramschmidt_lengths(rbasis)
    p = [0 for i in range(n+1)]
    solutions =[]
    v = [0 for i in range(n)]
    k = n-1 #indexing differs from that  of paper
    while True:
        p[k] = p[k+1] + alphas[k-1]*(v[k]**2+v[k])
        if p[k] <= R:
            if k == 0:
                solutions.append(tuple(v))
                v[k] = v[k]+1
            else: k,v[k] = k-1, 0
        else:
            k = k+1
            if k == n: return solutions
            else: v[k] = v[k]+1

def tes1(num_Bs=15, n=56,k=28,q= 521, seed = 710):
    fp.FPLLL.set_random_seed(seed)
    R_list = [(i+7)*40 for i in range(50)]
    total = [0 for i in range(len(R_list))]
    total_running_time = total
    for i in range(num_Bs):
        L = [[0 for l in range(n)] for j in range(n)]
        A = fp.IntegerMatrix(n,n)
        A.randomize("qary", k=k, q=q)
        fp.LLL.reduction(A)
        A.to_matrix(L)
        current = []
        runningtime = []
        for R in R_list:
            start_time = time.time()
            current = current + [len(enum_lowcost_fixed( R, L))]
            runningtime = runningtime+ [time.time()-start_time]
            print('completed [R, current basis, runningtime]', [R,i,time.time()-start_time])
        total = [total[j]+current[j] for j in range(len(current))]
        total_running_time = [total_running_time[j]+runningtime[j] for j in range(len(current))]
    average = [j/num_Bs for j in total]
    average_runningtime = [j/num_Bs for j in total_running_time]
    print('average: ', average)
    print('average running time: ',average_runningtime)
    print('R list: ', R_list)
    return average,average_runningtime, R_list


def main():
    M = [[1,9,9],[3,0,5],[7,7,7]] #Lattice basis
    G = [[1,9,9],[441/163,-432/163,383/163],[ 6300/1619,3080/1619,-3780/1619]] #gramschmidt
    x1 = [19, 392/163, 5600/1619]#coefficients (1,1,1)
    x2 = [11, 1265/163, 8820/1619]#coefficients (2,0,1)
    x3 = [18, -49/163, -700/1619] #coefficients (0,1,1)
    G2 = [[ 3/5, -4/5], [4,3]]
    lengths_list = [norm(G2[i]) for i in range(len(G2))]
    print('M: ',M)
    print('gramschmidt lengths: ',get_gramschmidt_lengths(M))
    print(lengths_list)
    print(enum_lowcost_fixed( 100, M))
    #_ = count_lc_assignments(f,20,3,lengths_list,15)
    #_ = enumeration_lowcost_assignments(f,3, lengths_list,100)
    _=enum_lowcost(fspec, 2,2, lengths_list)
    #for tag in _ :print(natural_partition( tag,M))
    print(_)
    #print(tagging_np([0 ,0 ,-1]))
    #tes1()
if __name__ == '__main__':
    main()
